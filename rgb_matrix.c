/*
 * Copyright (C) 2020 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    rgb_matrix.c
 *
 * @brief   Display images on the Adafruit 420 32x16 RGB LED matrix
 *
 * The program loads a set of binary image files, where each byte encodes one
 * basic colour from 0 to 7. The files are expected to have enough bytes for a
 * 32x16 image. Multiple files can be given and are cycled through.
 * The code works with the RaspberryPi on QNX, but can be easily adapted to
 * other boards and operating systems.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/neutrino.h>

/**
 * Symbolic names for GPIO connections.
 */
enum {
    // Row-select GPIO pins
    PIN_RS_A = 7,
    PIN_RS_B = 8,
    PIN_RS_C = 9,

    // Output/clock/latch GPIO pins
    PIN_OE = 2,
    PIN_CLK = 3,
    PIN_LAT = 4,

    // RGB GPIO pins
    PIN_R1 = 17,
    PIN_G1 = 18,
    PIN_B1 = 22,

    PIN_R2 = 23,
    PIN_G2 = 24,
    PIN_B2 = 25,
};

/**
 * RaspberryPi GPIO registers.
 */
enum
{
    REG_GPSET0 = 7,
    REG_GPCLR0 = 10,
    REG_GPLEV0 = 13,
    REG_GPEDS0 = 16,
    REG_GPEDS1 = 17,
    REG_GPREN0 = 19,
    REG_GPREN1 = 20,
    REG_GPFEN0 = 22,
    REG_GPFEN1 = 23,
    REG_GPHEN0 = 25,
    REG_GPHEN1 = 26,
    REG_GPLEN0 = 28,
    REG_GPLEN1 = 29,
    REG_GPPUD = 37,
    REG_GPPUDCLK1 = 38,
    REG_GPPUDCLK2 = 39,
};

/**
 * RaspberryPi System Timer registers.
 */
enum
{
    REG_STCS = 0,
    REG_STCLO = 1,
    REG_STCHI = 2,
    REG_STC0 = 3,
    REG_STC1 = 4,
    REG_STC2 = 5,
    REG_STC3 = 6,
};

/**
 * Number of bytes in an image buffer.
 */
#define IMAGE_SIZE          (32 * 16)

/**
 * Interval, in microseconds, for the timer interrupt.
 */
#define TIMER_INTR_USECS    100

/**
 * Base physical address for the RaspberryPi 4 peripherals.
 * A different address can be specified with the -a command-line argument.
 */
static uintptr_t base_paddr = 0xfe000000;

/**
 * Base virtual address of the mapped GPIO registers.
 */
static uint32_t volatile   *gpio_regs;

/**
 * Base virtual address of the mapped system timer registers.
 */
static uint32_t volatile   *timer_regs;

/**
 * Debug verbosity level.
 */
static unsigned             verbose;

/**
 * Image buffer read by the timer ISR.
 */
static uint8_t  image_buffer[IMAGE_SIZE];

/**
 * Programs the FSEL register for the given GPIO.
 * The value is between 0 and 7 and determines the function of the GPIO. Typical
 * values are 0 for input and 1 for output.
 * @param   gpio    Pin number
 * @param   value   New pin mode
 */
static inline void
gpio_set_select(unsigned const gpio, unsigned const value)
{
    unsigned const  reg = gpio / 10;
    unsigned const  off = (gpio % 10) * 3;
    gpio_regs[reg] &= ~(7 << off);
    gpio_regs[reg] |= (value << off);
}

/**
 * Turns on a pin in output mode.
 * @param   gpio    Pin number
 */
static inline void
gpio_set(unsigned const gpio)
{
    unsigned const  reg = REG_GPSET0 + gpio / 32;
    unsigned const  off = gpio % 32;
    gpio_regs[reg] = (1 << off);
}

/**
 * Turns off a pin in output mode.
 * @param   gpio    Pin number
 */
static inline void
gpio_clear(unsigned gpio)
{
    unsigned const  reg = REG_GPCLR0 + gpio / 32;
    unsigned const  off = gpio % 32;
    gpio_regs[reg] = (1 << off);
}

/**
 * Sets or clears a pin.
 * @param   gpio    Pin number
 * @param   value   0 to clear, any other value to set
 */
static inline void
gpio_write(unsigned const gpio, unsigned const value)
{
    if (verbose) {
        printf("gpio=%u value=%u\n", gpio, value);
    }

    if (value) {
        gpio_set(gpio);
    } else {
        gpio_clear(gpio);
    }
}

/**
 * Interrupt service routine for the system timer.
 * Displays the contents written by the previous ISR on the current lines and
 * then writes the contents of the next lines to draw.
 * @param   area    Ignored
 * @param   id      Ignored
 * @retuen  Always NULL
 */
static struct sigevent const *
timer_isr(void *area, int id)
{
    static unsigned row = 0;
    unsigned const  cur_time = timer_regs[REG_STCLO];

    if ((timer_regs[REG_STCS] & 2) == 0) {
        // Ignore spurious interrupts not generated by a match.
        return NULL;
    }

    // Select the current row to display the contents written by the previous
    // interrupt.
    gpio_write(PIN_RS_A, row & 1);
    gpio_write(PIN_RS_B, (row >> 1) & 1);
    gpio_write(PIN_RS_C, (row >> 2) & 1);

    // Advance to the next row.
    row = (row + 1) % 8;

    gpio_write(PIN_OE, 1);

    // Start row.
    gpio_write(PIN_LAT, 0);
    nanospin_ns(200);

    // Write the line contents for the active two rows.
    uint8_t * const line1 = &image_buffer[row * 32];
    uint8_t * const line2 = &image_buffer[(row + 8) * 32];
    for (unsigned col = 0; col < 32; col++) {
        gpio_write(PIN_CLK, 0);
        nanospin_ns(200);
        gpio_write(PIN_R1, line1[col]);
        gpio_write(PIN_R1, (line1[col] & 1));
        gpio_write(PIN_G1, (line1[col] & 2) >> 1);
        gpio_write(PIN_B1, (line1[col] & 4) >> 2);
        gpio_write(PIN_R2, (line2[col] & 1));
        gpio_write(PIN_G2, (line2[col] & 2) >> 1);
        gpio_write(PIN_B2, (line2[col] & 4) >> 2);
        gpio_write(PIN_CLK, 1);
        nanospin_ns(200);
    }

    // End row.
    gpio_write(PIN_LAT, 1);
    nanospin_ns(200);

    gpio_write(PIN_OE, 0);

    // Reload comparator.
    timer_regs[REG_STC1] = cur_time + TIMER_INTR_USECS;

    // Clear match.
    timer_regs[REG_STCS] = 2;

    return NULL;
}

/**
 * Maps the GPIO registers and initializes the output pins.
 * @return  true if successful, false otherwise
 */
static bool
init_gpios(void)
{
    static int const outputs[] = {
        PIN_RS_A,
        PIN_RS_B,
        PIN_RS_C,
        PIN_OE,
        PIN_CLK,
        PIN_LAT,
        PIN_R1,
        PIN_G1,
        PIN_B1,
        PIN_R2,
        PIN_G2,
        PIN_B2
    };

    // Map the GPIO registers.
    gpio_regs = mmap(0, __PAGESIZE, PROT_READ | PROT_WRITE | PROT_NOCACHE,
                     MAP_PHYS | MAP_SHARED, NOFD, base_paddr + 0x200000);
    if (gpio_regs == MAP_FAILED) {
        perror("mmap");
        return false;
    }

    // Initialize all output pins.
    for (unsigned i = 0; i < sizeof(outputs) / sizeof(outputs[0]); i++) {
        gpio_set_select(outputs[i], 1);
        gpio_write(outputs[i], 0);
    }

    return true;
}

/**
 * Sets up the system timer to fire for the first time and be serviced by the
 * timer ISR.
 * @return  true if successful, false otherwise
 */
static bool
init_timer(void)
{
    // Map the system timer registers.
    timer_regs = mmap(0, __PAGESIZE, PROT_READ | PROT_WRITE | PROT_NOCACHE,
                      MAP_PHYS | MAP_SHARED, NOFD, base_paddr + 0x3000);
    if (timer_regs == MAP_FAILED) {
        perror("mmap");
        return false;
    }

    // Attach the ISR.
    int const   timer_intr_id = InterruptAttach(97, timer_isr, NULL, 0, 0);
    if (timer_intr_id == -1) {
        perror("InterruptAttach");
        return false;
    }

    timer_regs[REG_STC1] = timer_regs[REG_STCLO] + TIMER_INTR_USECS;
    return true;
}

/**
 * Reads an image from a file into a newly allocated image array.
 * @param   path    The path to the file
 * @return  Image-sized array with the image data
 */
static uint8_t *
load_image(char const *path)
{
    int const fd = open(path, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "failed to open %s\n", path);
        return NULL;
    }

    uint8_t *image = malloc(IMAGE_SIZE);
    if (read(fd, image, IMAGE_SIZE) != IMAGE_SIZE) {
        close(fd);
        free(image);
        fprintf(stderr, "%s is not a valid image file\n", path);
        return NULL;
    }

    return image;
}

/**
 * Main program.
 * Loads the images specified on the command line and cycles through those.
 * @param   argc    Command-line argument count
 * @param   argv    Command-line argument array
 * @return  EXIT_SUCCESS/EXIT_FAILURE
 */
int
main(int argc, char **argv)
{
    for (;;) {
        int const opt = getopt(argc, argv, "a:v");
        if (opt == 'a') {
            base_paddr = strtoul(optarg, NULL, 0);
        } else if (opt == 'v') {
            verbose++;
        } else {
            break;
        }
    }

    // Load images from files.
    int const nimages = argc - optind;
    if (nimages <= 0) {
        fprintf(stderr, "Specify one or more image files\n");
        return EXIT_FAILURE;
    }

    uint8_t **images = malloc(sizeof(uint8_t *) * nimages);
    for (int i = 0; i < nimages; i++) {
        images[i] = load_image(argv[i + optind]);
        if (images[i] == NULL) {
            return EXIT_FAILURE;
        }
    }

    if (!init_gpios()) {
        return EXIT_FAILURE;
    }

    if (!init_timer()) {
        return EXIT_FAILURE;
    }

    // Enable output.
    gpio_write(PIN_OE, 0);

    int image_idx = 0;

    // Rotate through all images at one second intervals.
    for (;;) {
        sleep(1);
        memcpy(image_buffer, images[image_idx], IMAGE_SIZE);
        image_idx = (image_idx + 1) % nimages;
    }

    return EXIT_SUCCESS;
}
